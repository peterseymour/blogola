# Blogola blogging demo

## Installation

First clone the git repository

```
git clone https://bitbucket.org/peterseymour/blogola
```

Install virtualenv if not installed on your OS. For Ubuntu run

```
sudo apt-get install python-virtualenv
```

Configure the virtualenv

```
cd blogola
virtualenv venv --python=python3
```

Then enter the environment and confiure

```
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Finally run the server and visit [http://127.0.0.1:8000]

```
./manage.py runserver
```

The admin interface is available at [http://127.0.0.1:8000/admin]

Blog posts can be added from the admin interface, sign in as

```
Username: admin
Password: Bl0g0l@1
```

Sign in to add comments to posts.
The two users are

```
Username: reggie_stry
Password: P@ssword1

Username: honey_moon
Password: P@ssword1
```

The management command 'purge_history' removes posts older that 1 year and their comments.
