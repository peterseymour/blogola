from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.exceptions import PermissionDenied
from .models import Post, Comment


def index(request):
    posts = Post.objects.all().order_by('created')

    return render(request, "posts/posts.html", {'posts': posts})


def post(request, post_id):
    post = get_object_or_404(Post, id=post_id)

    return render(request, "posts/post.html", {'post': post})


@login_required
def comment(request, post_id):
    if request.method == "POST" and 'text' in request.POST:
        post = get_object_or_404(Post, id=post_id)

        comment = Comment(
            post=post,
            user=request.user,
            text=request.POST['text'],
        )

        comment.save()

        return JsonResponse(
            {
                'html': render_to_string(
                    "posts/comment.html", {'comment': comment},
                    request=request,
                )
            }
        )

    raise PermissionDenied


@login_required
def remove_comment(request, comment_id):
    if request.method == "POST":
        comment = get_object_or_404(Comment, id=comment_id)
        post_id = comment.post.id

        comment.delete()

        return redirect("posts:post", post_id=post_id)

    raise PermissionDenied
