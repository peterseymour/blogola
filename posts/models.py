from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    heading = models.CharField(max_length=256)
    text = models.CharField(max_length=16384)

    class Meta:
        ordering = ['created']


class Comment(models.Model):
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True)
    post = models.ForeignKey(
        Post,
        related_name="comments",
        on_delete=models.CASCADE)
    user = models.ForeignKey(User)
    text = models.CharField(max_length=1048)

    class Meta:
        ordering = ['created']
