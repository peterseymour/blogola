from django.core.management.base import BaseCommand, CommandError
from posts.models import Post
from django.utils.timezone import now
from datetime import timedelta


class Command(BaseCommand):
    help = 'Removes all blogs over a year old'

    def handle(self, *args, **options):
        threshold = now() - timedelta(days=365)

        posts = Post.objects.filter(created__lt=threshold)

        print("Deleting %d posts and their comments" % posts.count())

        posts.delete()
