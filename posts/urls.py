from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

app_name = 'posts'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^post/(?P<post_id>[0-9]+)/$', views.post, name='post'),
    url(r'^post/(?P<post_id>[0-9]+)/comment/$', views.comment, name='comment'),
    url(
        r'^comment/(?P<comment_id>[0-9]+)/remove/$',
        views.remove_comment,
        name='remove_comment'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
]
