from django.test import TestCase
from django.test import Client


class TestViews(TestCase):
    def test_posts_page(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_comment_not_found(self):
        c = Client()
        response = c.post('comment/1/remove/', {})
        self.assertEqual(response.status_code, 404)

    """More tests are needed to create posts
    and comments from signed in users"""
